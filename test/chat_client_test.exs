defmodule ChatClientTest do
  use ExUnit.Case, async: false

  def default_storage_module, do: Lemonchat.Storage.Redis

  def start_chat_client(room) do
    client_opts = [
      room: room,
      parent: self(),
      storage_module: default_storage_module
    ]
    {:ok, client} = Lemonchat.ChatClient.start_link(client_opts)
    client
  end

  def erase_storage(room) do
    {:ok, storage} = default_storage_module.start_link(room: room)
    storage |> default_storage_module.erase
  end

  setup do
    room = "testroom"
    client = start_chat_client(room)
    {:ok, client: client, room: room}
  end

  teardown context do
    erase_storage(context[:room])
  end

  test "receive message notification", context do
    text = "blabla"
    media = "none"
    client = context[:client]
    client |> Lemonchat.ChatClient.add text, media
    assert_receive {:messages, [ %{ text: ^text, media: ^media } ]}
  end

  test "receive message notification from other client", context do
    client1 = context[:client]
    spawn fn ->
      client2 = start_chat_client(context[:room])
      client2 |> Lemonchat.ChatClient.add "from 2 client", "none"
    end
    client1 |> Lemonchat.ChatClient.add "from 1 client", "none"
    messages = receive do
      {:messages, msgs} -> msgs
    end
    assert Enum.any?(messages, fn(msg) -> match? %{ text: "from 1 client" }, msg end)
    assert Enum.any?(messages, fn(msg) -> match? %{ text: "from 2 client" }, msg end)
  end

  test "get_from", context do
    client = context[:client]
    client |> Lemonchat.ChatClient.add 1, "message 1", "none"
    client |> Lemonchat.ChatClient.add 2, "message 2", "none"
    client |> Lemonchat.ChatClient.add 3, "message 3", "none"
    client |> Lemonchat.ChatClient.add 5, "message 5", "none"
    client |> Lemonchat.ChatClient.add 6, "message 6", "none"
    from1 = client |> Lemonchat.ChatClient.get_from 1
    assert length(from1) == 5
    from2 = client |> Lemonchat.ChatClient.get_from 2
    assert length(from2) == 4

    from3 = client |> Lemonchat.ChatClient.get_from 3
    assert length(from3) == 3

    from5 = client |> Lemonchat.ChatClient.get_from 5
    assert length(from5) == 2
    from4 = client |> Lemonchat.ChatClient.get_from 4
    assert from4 == from5
  end

  test "get_from returns messages only from specific room", context do
    client1 = context[:client]
    client2 = start_chat_client("anotheroom")
    client1 |> Lemonchat.ChatClient.add "message 1", "none"
    client1 |> Lemonchat.ChatClient.add "message 2", "none"
    client2 |> Lemonchat.ChatClient.add "message 3 (anotheroom)", "none"
    messages = client1 |> Lemonchat.ChatClient.get_from 1 # get all
    assert length(messages) == 2
    assert Enum.all? messages, fn(msg) -> not match? %{ message: "message 3 (anotheroom)" }, msg end
    erase_storage("anotheroom")
  end

  test "get_range", context do
    client = context[:client]
    client |> Lemonchat.ChatClient.add 1, "message 1", "none"
    client |> Lemonchat.ChatClient.add 2, "message 2", "none"
    client |> Lemonchat.ChatClient.add 3, "message 3", "none"

    messages = client |> Lemonchat.ChatClient.get_range 1, 3
    assert length(messages) == 3

    messages = client |> Lemonchat.ChatClient.get_range 2, 4
    assert length(messages) == 2
    assert Enum.any?(messages, fn(msg) -> match? %{ text: "message 2" }, msg end)
    assert Enum.any?(messages, fn(msg) -> match? %{ text: "message 3" }, msg end)

    messages = client |> Lemonchat.ChatClient.get_range 2, 3
    assert length(messages) == 2
  end

  test "message length validation", context do
    client = context[:client]
    text150 = String.duplicate("a", 150)
    result = client |> Lemonchat.ChatClient.add text150, "none"
    assert result == :ok
    result = client |> Lemonchat.ChatClient.add text150 <> "!", "none"
    assert result == {:error, :text_too_long}
  end

  test "emptiness validation", context do
    client = context[:client]
    result = client |> Lemonchat.ChatClient.add "text", nil
    assert result == :ok
    result = client |> Lemonchat.ChatClient.add nil, "media"
    assert result == :ok
    result = client |> Lemonchat.ChatClient.add "", ""
    assert result == {:error, :empty}
  end
end