defmodule Lemonchat.Mixfile do
  use Mix.Project

  def project do
    [ app: :lemonchat,
      version: "0.0.1",
      elixir: "~> 0.14",
      deps: deps ]
  end

  # Configuration for the OTP application
  def application do
    [mod: { Lemonchat, [] },
     applications: [:cowboy, :gproc] ]
  end

  # Returns the list of dependencies in the format:
  # { :foobar, git: "https://github.com/elixir-lang/foobar.git", tag: "0.1" }
  #
  # To specify particular versions, regardless of the tag, do:
  # { :barbat, "~> 0.1", github: "elixir-lang/barbat" }
  defp deps do
    [
      {:cowboy, "~> 0.9", github: "extend/cowboy", tag: "0.9.0"},
      {:ranch, "~> 0.9", github: "extend/ranch", ref: "master", override: true},
      {:cowlib, "~> 0.4", github: "extend/cowlib", ref: "master", override: true},
      {:bullet, github: "extend/bullet"},
      {:gun, github: "extend/gun"},

      {:poolboy, github: "devinus/poolboy", tag: "1.2.1"},
      {:jsxn, github: "talentdeficit/jsxn"},
      {:gproc, github: "uwiger/gproc"},

      {:exredis, github: "artemeff/exredis"},
      {:uuid, github: "okeuday/uuid"},

      # {:riakc, github: "basho/riak-erlang-client"},
      # {:riak_pool, github: "marshall-lee/riak_pool"},
    ]
  end
end
