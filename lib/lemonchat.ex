defmodule Lemonchat do
  use Application

  def start(_type, _args) do
    count_exredis = 10
    create_exredis_pool! count_exredis
    start_http!
    Lemonchat.Supervisor.start_link(count_exredis: count_exredis)
  end

  defp create_exredis_pool!(count) do
    :gproc_pool.new(:exredis_pool)
    Enum.each 1..count, fn (n) ->
      name = Lemonchat.ExredisWorker.name(n)
      :gproc_pool.add_worker(:exredis_pool, name)
    end
  end

  defp start_http! do
    dispatch = :cowboy_router.compile([
        {:_, [
               {"/scripts/[...]", :cowboy_static, {:priv_dir, :lemonchat, "static/scripts", [{:mimetypes, :cow_mimetypes, :web}]}},
               {"/styles/[...]", :cowboy_static, {:priv_dir, :lemonchat, "static/styles", [{:mimetypes, :cow_mimetypes, :web}]}},
               {"/images/[...]", :cowboy_static, {:priv_dir, :lemonchat, "static/images", [{:mimetypes, :cow_mimetypes, :web}]}},
               {"/stream/:room", :bullet_handler, handler: Lemonchat.RoomHandler},
               {"/", :cowboy_static, {:priv_file, :lemonchat, 'static/index.html'}},
               {"/room/:room", :cowboy_static, {:priv_file, :lemonchat, 'static/index.html'}}
        ]}
    ])
    http_port = binary_to_integer(System.get_env("PORT") || "7000")
    IO.puts "starting lemonchat instance on #{http_port}"
    case :cowboy.start_http(
        :lemonchat_http_listener, # Name
        100, # NbAcceptors
        [port: http_port], # TransOpts
        [env: [dispatch: dispatch]]# ProtoOpts
      ) do
      {:error, err} ->
        :timer.sleep 500
        start_http!
      _ ->
        :ok
    end
  end
end
