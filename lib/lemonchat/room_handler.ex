defmodule Lemonchat.RoomHandler do
  def init(_transport, req, opts, _active) do
    {room, req} = :cowboy_req.binding(:room, req, "index")

    client_opts = [
      room: room,
      parent: self
    ]
    {:ok, client} = Lemonchat.ChatClient.start_link(client_opts)

    state = %{
      time_start: Lemonchat.ChatClient.get_timestamp,
      client: client
    }

    {peer, req} = :cowboy_req.peer(req)
    {real_ip, req} = :cowboy_req.header("x-real-ip", req, nil)
    if real_ip do
      from_addr_str = real_ip
    else
      {from_addr, from_port} = peer
      from_addr_str = :inet.ntoa(from_addr)
    end

    IO.puts("starting stream pid=#{inspect self}, room=#{inspect room}, from=#{from_addr_str}")
    self |> Process.send_after :init, 1

    {:ok, req, state}
  end

  def stream(json, req, state) do
    data = :jsxn.decode(json)
    data = transform_data(data)
    case data do
      %{"cmd" => "get_range"} -> handle_get_range(data, req, state)
      %{"cmd" => "ping"} -> handle_ping(data, req, state)
      %{"cmd" => "put"} -> handle_put(data, req, state)
      %{"cmd" => "close"} -> {:shutdown, req, state}
    end
  end

  def info(:init, req, state) do
    init_data = %{
      time_start: state.time_start
    }
    do_notify("init", init_data, req, state)
  end

  def info({:messages, messages}, req, state) do
    messages = messages |> Enum.drop_while fn (msg) -> msg.time <= state.time_start end
    if Enum.any? messages do
      do_notify("messages", messages, req, state)
    else
      {:ok, req, state}
    end
  end

  def info(info, req, state) do
    IO.puts "unknown info received " <> inspect(info)
    {:ok, req, state}
  end

  def terminate(_req, state) do
    :ok
  end

  defp transform_data(data) when is_map(data) do
    data |> Enum.reduce %{}, fn ({key, value}, acc) ->
      Map.put(acc, key, transform_data(value))
    end
  end
  defp transform_data(data) when is_list(data) do
    data |> Enum.map fn value ->
      transform_data(value)
    end
  end
  defp transform_data(:null), do: nil
  defp transform_data(nil), do: :null # hack
  defp transform_data(data), do: data

  defp do_notify(name, data, req, state) do
    reply = %{
      type: "notify_" <> name,
      data: transform_data(data)
    }
    {:reply, :jsxn.encode(reply), req, state}
  end

  defp do_reply(id, data, req, state) do
    reply = %{
      type: "reply",
      id: id,
      data: transform_data(data)
    }
    {:reply, :jsxn.encode(reply), req, state}
  end

  defp do_reply_error(id, error, req, state) do
    reply = %{
      type: "reply",
      id: id,
      error: transform_data(error)
    }
    {:reply, :jsxn.encode(reply), req, state}
  end

  defp handle_get_range(data, req, state) do
    [time_start, time_stop] = data["args"]
    unless time_start do
      time_start = time_stop - 1000*60*30
    end
    messages = state.client |> Lemonchat.ChatClient.get_range time_start, time_stop
    do_reply(data["id"], messages, req, state)
  end

  defp handle_ping(data, req, state) do
    do_reply(data["id"], "pong", req, state)
  end

  defp handle_put(data, req, state) do
    [text, media] = data["args"]
    case state.client |> Lemonchat.ChatClient.add text, media do
      :ok ->
        do_reply(data["id"], true, req, state)
      {:error, error} ->
        do_reply_error(data["id"], error, req, state)
    end
  end
end