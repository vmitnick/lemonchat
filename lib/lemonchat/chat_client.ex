defmodule Lemonchat.ChatClient do
  use GenServer

  ###################################################################
  # api
  ###################################################################

  def start_link(args) do
    GenServer.start_link(__MODULE__, args, [])
  end

  def add(pid, time, text, media) do
    GenServer.call(pid, {:add, time, text, media})
  end

  def add(pid, text, media) do
    GenServer.call(pid, {:add, text, media})
  end

  def get_to(pid, time) do
    get_range(pid, 0, time)
  end

  def get_from(pid, time) do
    get_range(pid, time, 4294967295000)
  end

  def get_range(pid, time_start, time_end) do
    GenServer.call(pid, {:get_range, time_start, time_end})
  end

  ###################################################################
  # gen_server callbacks
  ###################################################################

  def init(args) do
    Process.flag :trap_exit, true
    room = args[:room]

    # make storage process
    storage_module = args[:storage_module] || Lemonchat.Storage.Redis
    {:ok, storage} = storage_module.start_link(room: room)

    state = %{
      storage_module: storage_module,
      storage: storage,
      message_buffer: [],
      room: room,
      parent: args[:parent]
    }
    :gproc_ps.subscribe(:l, {:chat_msg, state.room})
    super state
  end

  def handle_call({:add, time, text, media}, _from, state) do
    text_str = to_string(text)
    media_str = to_string(media)
    cond do
      String.length(text_str) > 150 ->
        {:reply, {:error, :text_too_long}, state}
      String.length(text_str) == 0 and String.length(media_str) == 0 ->
        {:reply, {:error, :empty}, state}
      true ->
        room = state.room
        message = %{text: text, media: media, time: time}
        message = state.storage |> state.storage_module.put message
        :gproc_ps.publish(:l, {:chat_msg, room}, message)
        {:reply, :ok, state}
    end
  end

  def handle_call({:add, text, media}, _from, state) do
    time = get_timestamp
    handle_call({:add, time, text, media}, _from, state)
  end

  def handle_call({:get_range, time_start, time_stop}, _from, state) do
    messages = state.storage |> state.storage_module.get_range time_start, time_stop
    {:reply, messages, state}
  end

  def handle_info(:flush_message_buffer, state) do
    # send buffer to parent
    send state.parent, {:messages, state.message_buffer}
    # empty message buffer
    state = Map.put(state, :message_buffer, [])
    {:noreply, state}
  end

  def handle_info({:gproc_ps_event, {:chat_msg, room}, message}, state) do
    if state.room == room do
      unless Enum.any?(state.message_buffer) do
        Process.send_after(self(), :flush_message_buffer, 20)
      end
      state = Map.put(state, :message_buffer, [ message | state.message_buffer ])
    end
    {:noreply, state}
  end

  def terminate(_reason, state) do
    # :gproc_ps.unsubscribe(:l, {:chat_msg, state.room})
    :ok
  end

  ###################################################################

  def get_timestamp do
    {mega_seconds, seconds, micro_seconds} = :erlang.now()
    (mega_seconds * 1000000 + seconds) * 1000 + div(micro_seconds, 1000)
  end
end