defmodule Lemonchat.Storage do
  
  @doc false
  defmacro __using__(_) do
    quote location: :keep do
      use GenServer

      ###################################################################
      # api
      ###################################################################

      def start_link(args) do
        GenServer.start_link(__MODULE__, args, [])
      end

      def put(client, message) do
        GenServer.call(client, {:put, message})
      end

      def get_range(client, time_start, time_stop) do
        GenServer.call(client, {:get_range, time_start, time_stop})
      end

      def erase(client) do
        GenServer.call(client, :erase)
      end
    end
  end

  def sort(messages) do
    messages |> Enum.sort fn (msg1, msg2) -> msg1.time > msg2.time end
  end
end