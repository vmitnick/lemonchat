defmodule Lemonchat.ExredisWorker.Supervisor do
  use Supervisor

  def start_link(args) do
    Supervisor.start_link(__MODULE__, args)
  end

  def init(args) do
    count = args[:count]

    children = Enum.map 1..count, fn (n) ->
      name = Lemonchat.ExredisWorker.name(n)
      worker(Lemonchat.ExredisWorker, [[name: name]], [id: name])
    end

    supervise(children, strategy: :one_for_one)
  end
end

