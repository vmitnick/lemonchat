defmodule Lemonchat.Storage.Redis do
  use Lemonchat.Storage

  ###################################################################
  # gen_server callbacks
  ###################################################################

  def init(args) do
    uuid = :uuid.new(self)
    state = %{
      room: args[:room],
      worker_name: Lemonchat.ExredisWorker.pick_name,
      uuid: uuid
    }
    super state
  end

  def handle_call({:put, message}, _from, state) do
    redis = Lemonchat.ExredisWorker.get_client(state.worker_name)
    uuid = state.uuid |> :uuid.get_v1
    id = :uuid.uuid_to_string(uuid)
    id_key = msg_id_key(id)
    data = message |> :erlang.term_to_binary
    room_key = room_key(state)
    result = redis |> Exredis.query_pipe [
      ["SET", id_key, data],
      ["EXPIRE", id_key, 60*30],
      ["ZADD", room_key, message.time, id]
    ]
    case result do
      {:error, error} ->
        {:stop, :shutdown, state}
      _ ->
        {:reply, message, state}
    end
  end

  def handle_call({:get_range, time_start, time_stop}, _from, state) do
    redis = Lemonchat.ExredisWorker.get_client(state.worker_name)
    room_key = room_key(state)
    ids = redis |> Exredis.query ["ZREVRANGEBYSCORE", room_key, time_stop, time_start, "LIMIT", 0, 15]
    ids = ids |> Enum.reverse
    messages = if Enum.any? ids do
      id_keys = Enum.map ids, &msg_id_key(&1)
      list = redis |> Exredis.query ["MGET" |  id_keys]
      pairs = Enum.zip ids, list
      pairs = pairs |> Enum.filter fn ({id, data}) -> data != :undefined end
      pairs |> Enum.map fn({id, data}) ->
        message = data |> :erlang.binary_to_term
        Map.put message, :id, id
      end
    else
      []
    end
    {:reply, messages, state}
  end

  def handle_call(:erase, _from, state) do
    redis = Lemonchat.ExredisWorker.get_client(state.worker_name)
    room_key = room_key(state)
    ids = redis |> Exredis.query ["ZRANGE", room_key, 0, -1]
    redis |> Exredis.query ["DEL", room_key]
    if Enum.any? ids do
      redis |> Exredis.query ["DEL" | ids]
    end
    {:reply, :ok, state}
  end

  def handle_call(something, _from, state) do
    :io.format("SOMETHING: ~w~n", [something])
    {:reply, something, state}
  end

  def terminate(_reason, state) do
    :ok
  end

  ###################################################################
  # private stuff
  ###################################################################

  defp msg_id_key(id) when is_list(id) do
    'msg#' ++ id
  end

  defp msg_id_key(id) when is_bitstring(id) do
    "msg#" <> id
  end

  defp room_key(state) do
    room = state.room
    "chat#" <> room
  end

end