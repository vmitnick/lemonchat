defmodule Lemonchat.Storage.Riak do
  use Lemonchat.Storage

  ###################################################################
  # gen_server callbacks
  ###################################################################

  def handle_call({:put, room, message}, _from, state) do
    obj = message_to_obj(message, bucket_name(room))
    {:ok, obj} = RiakPool.put(obj)
    message = obj_to_message(obj)
    {:reply, message, state}
  end

  # RiakPool.get_index_range(bucket, {:integer_index, 'time'}, from, to)

  ###################################################################
  # private stuff
  ###################################################################

  defp bucket_name(state) do
    room = state.room
    "chat_" <> room
  end

  defp message_to_obj(message, bucket) do
    data = :erlang.term_to_binary({message.text, message.media})
    obj = :riakc_obj.new(bucket, :undefined, data)
    md = :riakc_obj.get_update_metadata(obj)
    md = :riakc_obj.set_secondary_index(md, {{:integer_index, 'time'}, [message.time]})
    obj = :riakc_obj.update_metadata(obj, md)
    obj
  end

  defp obj_to_message(obj) do
    id = :riakc_obj.key(obj)
    data = :riakc_obj.get_value(obj)
    {text, media} = :erlang.binary_to_term(data)
    md = :riakc_obj.get_metadata(obj)
    [time] = :riakc_obj.get_secondary_index(md, {:integer_index, 'time'})
    %{id: id, text: text, media: media, time: time}
  end
end