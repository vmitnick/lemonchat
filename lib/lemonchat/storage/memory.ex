defmodule Lemonchat.Storage.Memory do
  use Lemonchat.Storage

  ###################################################################
  # gen_server callbacks
  ###################################################################

  def init(args) do
    Process.flag :trap_exit, true
    try do
      :ets.new(:memchat, [:named_table, :public, :ordered_set])
    rescue
      ArgumentError -> 
        :memchat
    end
    room = args[:room]
    state = %{ room: room }
    super state
  end

  def handle_call({:put, message}, _from, state) do
    key = { state.room, message.time }
    value = { message.text, message.media }
    unless :ets.insert_new(:memchat, {key, value}) do
      message = Map.put(message, :time, message.time + 1)
      handle_call({:put, message}, _from, state)
    else
      {:reply, message, state}
    end
  end

  def handle_call({:get_range, time_start, time_stop}, _from, state) do
    keys = get_keys_range(:ets.next(:memchat, { state.room, time_start-1 }), time_stop, state.room)
    messages = Enum.map keys, fn(key) ->
      { {_, time}, {text, media} } = List.keyfind(:ets.lookup(:memchat, key), key, 0)
      message = %{ time: time, text: text, media: media }
    end
    messages = messages |> Enum.reverse
    {:reply, messages, state}
  end

  def handle_call(:erase, _from, state) do
    keys = get_keys_range(:ets.next(:memchat, { state.room, 0 }), 4294967295000, state.room)
    Enum.each keys, fn(key) -> :ets.delete(:memchat, key) end
    {:reply, :ok, state}
  end

  def terminate(_reason, state) do
    :ok
  end

  ###################################################################
  # private stuff
  ###################################################################

  defp get_keys_range(key, stop, room, list \\ []) do
    case key do
      { ^room, time } ->
        if time <= stop do
          next_key = :ets.next(:memchat, key)
          get_keys_range(next_key, stop, room, [key | list])
        else
          list
        end
      _ ->
        list
    end
  end
end