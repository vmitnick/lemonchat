defmodule Lemonchat.ExredisWorker do
  use GenServer

  ###################################################################
  # API
  ###################################################################

  def start_link(args) do
    GenServer.start_link(__MODULE__, args)
  end

  def pick_name do
    {_, _, [:gproc_pool, _, n, name]} = :gproc_pool.pick(:exredis_pool)
    name
  end

  def get_client(name) do
    case :gproc_pool.whereis_worker(:exredis_pool, name) do
      :undefined -> nil
      pid -> GenServer.call(pid, :get_client)
    end
  end

  ###################################################################
  # GenServer callbacks
  ###################################################################

  def init(args) do
    name = args[:name]
    :gproc_pool.connect_worker(:exredis_pool, name)
    state = %{ name: name }
    super(state)
  end

  def handle_call(:get_client, args, state) do
    unless Map.has_key? state, :client do
      client = Exredis.start
      ref = Process.monitor(client)
      state = Map.put(state, :client, client)
      state = Map.put(state, :client_ref, ref)
    end
    {:reply, state.client, state}
  end

  def handle_info({:DOWN, ref, :process, pid, reason}, state) do
    if ref == state.client_ref do
      state = Map.delete(state, :client)
      state = Map.delete(state, :client_ref)
    end
    {:noreply, state}
  end

  def terminate(reason, state) do
    :gproc_pool.disconnect_worker(:exredis_pool, state.name)
    if Map.has_key? state, :client do
      state.client |> Exredis.stop
    end
    :ok
  end

  ###################################################################
  # helpers
  ###################################################################

  def name(n) do
    "exredis-#{n}"
  end
end