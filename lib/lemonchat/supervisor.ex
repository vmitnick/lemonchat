defmodule Lemonchat.Supervisor do
  use Supervisor

  def start_link(args) do
    Supervisor.start_link(__MODULE__, args)
  end

  def init(args) do
    count_exredis = args[:count_exredis]
    children = [
      supervisor(Lemonchat.ExredisWorker.Supervisor, [[count: count_exredis]])
    ]

    supervise(children, strategy: :one_for_one)
  end
end
