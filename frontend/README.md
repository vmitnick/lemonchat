
Chat Frontend
=============

Technology used: [yeoman](http://yeoman.io), [grunt](http://gruntjs.com), [bower](http://bower.io), [ember](http://emberjs.com), [coffeescript](http://coffeescript.org).

Contribute
----------

1. `git clone` this
2. Install required node.js modules:
    * `npm install -g grunt-cli`
    * `npm install -g bower`
    * `npm install -g coffee-script`
    * `npm install -g coffeelint`
3. Install required Sublime 3 plugins:
    * [SublimeLinter](https://sublime.wbond.net/packages/SublimeLinter)
    * [Sublime​Linter-coffeelint](https://sublime.wbond.net/packages/SublimeLinter-coffeelint)
    * [Better CoffeeScript](https://sublime.wbond.net/packages/Better%20CoffeeScript)
    * [Handlebars](https://sublime.wbond.net/packages/Handlebars)
4. Read [~~JavaScript~~ CoffeeScript styleguide](https://github.com/styleguide/javascript) (camelCase, soft-tabs, two space indent, etc)
5. Configure development server
    * `iex -S mix` or `mix --no-halt` runs websocket server on port 7000
    * `grunt server` runs livereload server with static on port 9000
    * nginx aggregates these two upstreams.
    * `/etc/nginx.conf`:
      ```
          map $http_upgrade $connection_upgrade {
              default upgrade;
              ''      close;
          }
          server {
              listen       80;

              location /stream {
                  proxy_pass http://127.0.0.1:7000/stream;
                  proxy_http_version 1.1;
                  proxy_set_header Upgrade $http_upgrade;
                  proxy_set_header Connection $connection_upgrade;
              }
              
              rewrite ^/\w+$ /index.html;

              location / {
                  proxy_pass http://127.0.0.1:9000;
              }
          }
      ```
  * ahah
