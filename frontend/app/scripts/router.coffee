Chat.Router.reopen
  location: "history"

Chat.Router.map ->
  @route "room", path: ""
  @route "room", path: "room/:name"
  @route "join", path: "join"
  @route "about", path: "about"
