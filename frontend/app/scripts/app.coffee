require 'scripts/translations'

Chat = window.Chat = Ember.Application.create()

# Order and include as you please.
require 'scripts/controllers/*'
require 'scripts/store'
require 'scripts/models/*'
require 'scripts/routes/*'
require 'scripts/views/*'
require 'scripts/helpers/*'
require 'scripts/components/*'
require 'scripts/router'
require 'scripts/room_client'