
Chat.BulletClient = Ember.Object.extend Ember.Evented,
  countOpen: 0

  init: ->
    host = @get("host") || window.location.host
    path = @get("path") || ""
    @set "host", host
    @set "path", path
    unless @get("bullet")
      @startBullet()
      
  startBullet: ->
    host = @get "host"
    path = @get "path"
    bullet = $.bullet "ws://#{host}/#{path}"
    bullet.onopen = =>
      @trigger "open"
      @incrementProperty "countOpen"
      if @get("countOpen") > 5
        @get("bullet").close()
        @startBullet()
    bullet.ondisconnect = => @trigger "disconnect"
    bullet.onclose = => @trigger "close"
    bullet.onmessage = (e) => @trigger "data", e.data
    bullet.onheartbeat = => @trigger "heartbeat"
    @set "countOpen", 0
    @set 'bullet', bullet

  send: (data) ->
    unless typeof data == String
      data = JSON.stringify(data)
    if @get('bullet').send(data) == false
      false
    else
      true

  close: ->
    @get('bullet').close()

