
Ember.Handlebars.registerBoundHelper 'moment-format', (date, format) ->
  moment(date).format(format)

Ember.Handlebars.registerBoundHelper 'moment-from-now', (date) ->
  moment(date).fromNow()

Ember.Handlebars.registerBoundHelper 'moment-calendar', (date) ->
  moment(date).calendar()
