
Ember.Handlebars.registerBoundHelper 'text-format', (text) ->
  protocolRegex = /\:\/\//
  urlRegex =  /(?:(?:ht|f)tp(?:s?)\:\/\/|~\/|\/)?(?:\w+:\w+@)?((?:(?:[-\w\d{1-3}]+\.)+(?:com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|edu|co\.uk|ac\.uk|it|fr|tv|museum|asia|local|travel|[a-z]{2}))|((\b25[0-5]\b|\b[2][0-4][0-9]\b|\b[0-1]?[0-9]?[0-9]\b)(\.(\b25[0-5]\b|\b[2][0-4][0-9]\b|\b[0-1]?[0-9]?[0-9]\b)){3}))(?::[\d]{1,5})?(?:(?:(?:\/(?:[-\w~!$+|.,=]|%[a-f\d]{2})+)+|\/)+|\?|#)?(?:(?:\?(?:[-\w~!$+|.,*:]|%[a-f\d{2}])+=?(?:[-\w~!$+|.,*:=]|%[a-f\d]{2})*)(?:&(?:[-\w~!$+|.,*:]|%[a-f\d{2}])+=?(?:[-\w~!$+|.,*:=]|%[a-f\d]{2})*)*)*(?:#(?:[-\w~!$ |\/.,*:;=]|%[a-f\d]{2})*)?/gi
  linebreakRegex = /([\r\n|\n|\r]+)/gm

  text = $.trim(text)
  text = Handlebars.Utils.escapeExpression(text)
  text = text.replace urlRegex, (str) ->
    url = if str.match(protocolRegex)
      str
    else
      "//" + str
    "<a href='#{url}'>#{str}</a>"
  text = text.replace linebreakRegex, "<br />"
  text.htmlSafe()

Ember.Handlebars.registerHelper 't-text-format', (key, options) ->
  text = Ember.I18n.t(key)
  Ember.Handlebars.helpers["text-format"].call(this, text, options)