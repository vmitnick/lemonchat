Chat.ImgDomComponent = Ember.Component.extend
  tagName: "img"
  attributeBindings: ["src"]
  
  willInsertElement: ->
    from = @get("from")
    $holder = $("#" + from)
    @set "src", $holder.attr("src")