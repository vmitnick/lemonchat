# global hack:
$(window).on 'orientationchange', ->
  GumHelper.stopVideoStreaming()

require 'scripts/video_shooter'

Chat.WebcamHolderComponent = Ember.Component.extend Chat.VideoShooter,
  classNames: ["webcam-holder"]
  attributeBindings: ["style"]

  style: (->
    width = @get "width"
    height = @get "height"
    "width=#{width} height=#{height}"
  ).property("width", "height")

  resizeObserver: (->
    if @get('resizeToFit')
      @doResizeToFit()
    else
      @doResizeStupid()
  ).observes('videoElement', 'width', 'height', 'resizeToFit')

  sizeChangeObserver: (->
    Ember.run.once(this, 'doRealResize')
  ).observes('videoElement', 'realHeight', 'realWidth')

  didInsertElement: ->
    onOrientationChangeProxy = $.proxy ->
      Ember.run(this, 'onOrientationChange')
    , this
    @set 'onOrientationChangeProxy', onOrientationChangeProxy
    $(window).on 'orientationchange', onOrientationChangeProxy

    GumHelper.startVideoStreaming (err, stream, videoElement, videoWidth, videoHeight) =>
      if err
        @sendAction 'error'
      else
        @setProperties
          videoHeight: videoHeight
          videoWidth: videoWidth
          videoElement: videoElement
        @$().html(videoElement)
        $(videoElement).fadeIn('slow')
        videoElement.play()
        @sendAction 'start', this

  willClearRender: ->
    $(window).off 'orientationchange', @get('onOrientationChangeProxy')

  doResizeToFit: ->
    videoElement = @get 'videoElement'
    if videoElement?
      width = @get 'width'
      height = @get 'height'
      videoWidth = @get 'videoWidth'
      videoHeight = @get 'videoHeight'
      realWidth = width
      realHeight = height

      if videoWidth > videoHeight
        realWidth = Math.round(videoWidth * (height / videoHeight))
      else
        realHeight = Math.round(videoHeight * (width / videoWidth))

      @setProperties(realWidth: realWidth, realHeight: realHeight)

  doResizeStupid: ->
    videoElement = @get 'videoElement'
    if videoElement?
      width = @get 'width'
      height = @get 'height'
      @setProperties(realWidth: realWidth, realHeight: realHeight)

  doRealResize: ->
    videoElement = @get 'videoElement'
    if videoElement?
      realHeight = @get 'realHeight'
      realWidth = @get 'realWidth'

      videoElement.height = realHeight
      videoElement.width = realWidth
      $(videoElement).css
        height: realHeight + 'px'
        width: realWidth + 'px'

  onOrientationChange: ->
    @set 'videoElement', undefined
    @sendAction 'stop'
    @rerender()