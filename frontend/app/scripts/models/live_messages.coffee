
Chat.LiveMessages = Ember.ArrayProxy.extend
  init: ->
    @set "content", Ember.A([])
    @_super()

    room = @get("room")
    client = Chat.RoomClient.create(room: room)
    client.on "messages", @pushObjects.bind(this)
    client.on "open", -> console.log "open"
    client.on "close", -> console.log "close"
    client.on "disconnect", -> console.log "disconnect"
    client.on "heartbeat", -> console.log "heartbeat"
    @set "client", client