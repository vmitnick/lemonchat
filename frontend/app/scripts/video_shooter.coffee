
Chat.VideoShooter = Ember.Mixin.create
  numFrames: 15
  interval: 0.2

  getShot: ->
    videoElement = @get 'videoElement'
    videoWidth = @get 'videoWidth'
    videoHeight = @get 'videoHeight'
    width = @get 'width'
    height = @get 'height'
    realWidth = @get 'realWidth'
    realHeight = @get 'realHeight'
    interval = @get 'interval'
    numFrames = @get 'numFrames'
    
    deferred = $.Deferred (defer) =>
      videoCropWidth = 0
      videoCropHeight = 0
      if videoWidth > videoHeight
        videoCropWidth = Math.round((width - realWidth) * (videoHeight / height))
      else
        videoCropHeight = Math.round((height - realHeight) * (videoWidth / width))
      sWidth = videoWidth - videoCropWidth
      sHeight = videoHeight - videoCropHeight
      sX = Math.floor(videoCropWidth / 2)
      sY = Math.floor(videoCropHeight / 2)

      canvas = document.createElement('canvas')
      canvas.width = width
      canvas.height = height
      context = canvas.getContext '2d'

      ag = new Animated_GIF(workerPath: '/scripts/animated_gif.worker.js')
      ag.setSize(width, height)
      ag.setDelay(interval)

      framesLeft = numFrames

      captureFrame = =>
        context.drawImage(videoElement, sX, sY, sWidth, sHeight, 0, 0, width, height)
        ag.addFrameImageData(context.getImageData(0, 0, width, height))

        framesLeft = framesLeft - 1
        progress = (numFrames - framesLeft) / numFrames
        defer.notifyWith(this, [progress])
        if framesLeft > 0
          Ember.run.later captureFrame, interval*1000
        else
          ag.getBase64GIF (image) ->
            ag.destroy()
            defer.resolve(image)
      Ember.run.next captureFrame

    return deferred.promise()
