Chat.RoomView = Ember.View.extend
  observeIsPreloading: (->
    if @get("controller.isPreloading")
      progressBar = progressJs()
      progressBar.setOptions(theme: "yellow")
      progressBar.autoIncrease(4, 500)
      progressBar.start()
      @set "progressBarPreloading", progressBar
    else
      if @get("progressBarPreloading")?
        @get("progressBarPreloading").end()
        @set "progressBarPreloading", undefined
  ).observes("controller.isPreloading")

  observeShooting: (->
    if @get("controller.isShooting")
      progressBar = progressJs(@$(".webcam-container"))
      progressBar.setOptions(bottom: true, theme: "red")
      progressBar.start()
      @set "progressBarShoot", progressBar
    else
      if @get("progressBarShoot")?
        @get("progressBarShoot").end()
        @set "progressBarShoot", undefined
  ).observes("controller.isShooting")

  observeProgress: (->
    if @get("controller.isShooting")
      @get("progressBarShoot").set(@get("controller.progress"))
  ).observes("controller.progress")

  observeIsSendBlocked: (->
    $bubble = @$(".form-container .bubble")
    if $bubble?
      if @get("controller.isSendBlocked")
        $bubble.addClass("disabled")
      else
        $bubble.removeClass("disabled")
  ).observes("controller.isSendBlocked")

  observeIsSendError: (->
    $bubble = @$(".form-container .bubble")
    if $bubble?
      if @get("controller.isSendError")
        $bubble.addClass("error")
      else
        $bubble.removeClass("error")
  ).observes("controller.isSendError")

  observeNewMessages: (->
    Ember.run.next =>
      @scrollToBottom()
  ).observes("controller.@each")

  scrollToBottom: ->
    $content = @$(".room-content")
    if $content?
      $content.animate(scrollTop :$content.get(0).scrollHeight)

  didInsertElement: ->
    @$("textarea").keydown (e) =>
      prevent = false

      # 'enter' key pressed
      if e.keyCode == 13 or e.keyCode == 10
        unless e.ctrlKey or e.shiftKey
          prevent = true
          Ember.run => @get("controller").send "sendMessage"

      # text entered
      if 46 < e.keyCode < 91
        prevent = true if @get("controller.cannotEdit")

      if prevent
        e.preventDefault()
      else
        # something entered
        if @get("controller.isSendError")
          Ember.run => @get("controller").send "resetError"