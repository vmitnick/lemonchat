
Chat.JoinController = Ember.Controller.extend
  roomName: ""

  roomUrl: (->
    roomName = @get "roomName"
    if roomName
      "chat.lemonspace.me/room/#{roomName}"
    else
      ""
  ).property("roomName")

  isValid: (->
    roomName = @get "roomName"
    roomName.match(/^[a-z0-9_-]+$/i) != null
  ).property("roomName")