
Chat.RoomController = Ember.ArrayController.extend
  sortBy: ["time"]

  newMessageText: ""
  isSendBlocked: null

  state: (->
    @get("model")?.get("client.state")
  ).property("model.client.state")

  isPreloading: (->
    @get("state") == "opened"
  ).property("state")

  client: (->
    @get("model")?.get("client")
  ).property("model")

  shootTime: (->
    if @get("isShooting")
      seconds = @get "shootSeconds"
      if seconds < 10
        "0:0#{seconds}"
      else
        "0:#{seconds}"
    else
      ""
  ).property("shootSeconds", "isShooting")

  messageLengthLeft: (->
    message = @get("newMessageText") || ""
    150 - message.length
  ).property("newMessageText")

  cannotEdit: (->
    @get("messageLengthLeft") <= 0
  ).property("messageLengthLeft")

  cannotSend: (->
    message = @get("newMessageText") || ""
    message.length < 2
  ).property("newMessageText")

  doSend: ->
    @set "isSending", true
    client = @get("client")
    client.put(@get("newMessageText"), @get("newMessageMedia"))
      .then =>
        @set "newMessageText", ""
      .catch (error) =>
        @set "error", error
        @set "isSendError", true
      .finally =>
        @set "isSending", false
        @set "isSendBlocked", null

  doSendWithShot: ->
    @set "isShooting", true
    @set "shootSeconds", 0

    timerFunc = =>
      @incrementProperty "shootSeconds"
      if @get("isShooting")
        Ember.run.later(timerFunc, 1000)

    Ember.run.later(timerFunc, 1000)

    @get("webcam").getShot()
      .progress (p) =>
        @set "progress", p * 100
      .done (media) =>
        @set "newMessageMedia", media
        @doSend()
      .fail =>
        @set "isSendBlocked", null
      .always =>
        @set "isShooting", false

  actions:
    sendMessage: ->
      unless @get("isSendBlocked")
        if @get("cannotSend")
          @set "isSendError", true
        else
          @set "isSendBlocked", 1
          if @get("webcam")
            @doSendWithShot()
          else
            @set "newMessageMedia", null
            @doSend()

    resetError: ->
      @set "error", null
      @set "isSendError", false

    startStreaming: (webcam) ->
      @set "webcam", webcam
    stopStreaming: ->
      @set "webcam", undefined
    errorStreaming: ->
      @set "webcam", undefined