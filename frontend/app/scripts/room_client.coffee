
require "scripts/bullet_client"

Chat.RoomClient = Chat.BulletClient.extend
  id: 0
  lastMessageTime: null

  init: ->
    @set "path", "stream/#{@get('room')}"
    @_super()

    @on "open", @handleOpen.bind(this)
    @on "disconnect", @handleDisconnect.bind(this)
    @on "data", @handleReply.bind(this)
    @on "heartbeat", @doRequest.bind(this, "ping")

  triggerMessages: (messages) ->
    if messages.length > 0
      messages = messages.map((msg) -> Chat.Message.create(msg)) # .sortBy("time")
      lastMessage = messages.get("lastObject")
      @set "lastMessageTime", lastMessage.get("time")
      @trigger "messages", messages

  handleDisconnect: ->
    @set "state", "closed"

  handleOpen: ->
    @set "mailbox", Ember.A([])
    @set "state", "opened"

  handleInitialMessages: (data) ->
    @triggerMessages(data)
    @triggerMessages(@get("mailbox"))
    @set "mailbox", Ember.A([])
    @set "state", "ready"

  handleReply: (reply) ->
    reply = $.parseJSON(reply)
    switch reply.type
      when "notify_init"
        console.log("notify_init")
        @set "timeInitial", reply.data.time_start
        @getGap().then @handleInitialMessages.bind(this)
      when "notify_messages"
        console.log("notify_messages")
        if @get("state") == "opened"
          @get("mailbox").pushObjects(reply.data) # postpone messages
        else if @get("state") == "ready"
          @triggerMessages(reply.data)
      when "reply"
        @trigger "reply_#{reply.id}", reply

  doRequest: (cmd, args...) ->
    new Ember.RSVP.Promise (resolve, reject) =>
      id = @incrementProperty("id")
      Ember.run.later =>
        ret = @send
          id: id
          cmd: cmd
          args: args
        unless ret
          @trigger "reply_#{id}", error: { reason: "not_connected" }
      @one "reply_#{id}", (reply) ->
        if reply.error
          console.log reply.error
          reject(reply.error)
        else
          resolve(reply.data)

  getRange: (timeStart, timeStop) ->
    @doRequest("get_range", timeStart, timeStop)

  getGap: ->
    timeStart = if @get("lastMessageTime")
      @get("lastMessageTime") + 1
    else
      null
    timeStop = @get("timeInitial")
    @getRange(timeStart, timeStop)

  put: (text, media) ->
    @doRequest("put", text, media)
