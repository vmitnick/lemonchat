
language = navigator.language || navigator.userLanguage || "en"
mainLanguage = language.substr(0,2)

enTranslations =
  menu:
    join_room: "Join room"
    about: "About"
  join:
    prompt: "Room"
    invalid: "Error: unacceptable symbols"
  about:
    one:
      header: "Welcome!"
      par1: "Lemonspace chat is a clone of meatspaces
        (github.com/meatspaces/meatspace-chat)
        created in non-commercial purposes for some small community of people
        to have fun together."
      par2: "However, we always welcome new friends."
    two:
      header: "Rooms"
      par1: "It is possible to chat in rooms other than main. You can even create your own."
      par2: "These rooms are available at URLs like
        chat.lemonspace.me/room/mybloodyenterprise
        where mybloodyenterprise is the room name.
        To create your own you should only join a room with a particular name."
    three:
      header: "Can i contribute?"
      par1: "You can but for now i don't know how.
        More information will be available soon and source code will be pushed to github too."

ruTranslations =
  menu:
    join_room: "Войти в комнату"
    about: "Информация"
  join:
    prompt: "Название комнаты"
    invalid: "Ошибка: недопустимые символы"
  about:
    one:
      header:    "Добро пожаловать!"
      par1: "Чат Lemonspace - это клон чата meatspaces
        (github.com/meatspaces/meatspace-chat),
        созданный в личных некоммерческих целях для небольшой компании людей,
        чтобы им было весело."
      par2: "Тем не менее, мы всегда рады новым знакомым."
    two:
      header:  "Комнаты"
      par1: "Помимо главной комнаты, могут быть еще и другие. Вы даже можете создать свою собственную."
      par2: "Эти комнаты доступны по адресам вида
        chat.lemonspace.me/room/mybloodyenterprise ,
        где mybloodyenterprise - это название комнаты.
        Чтобы создать свою комнату,
        необходимо просто выполнить вход в комнату с нужным названием."
    three:
      header: "Могу ли я помочь?"
      par1: "Можете, но я пока не знаю как.
        Позже здесь появится больше информации об этом, а также будут выложены исходники."

Em.I18n.translations =
  switch mainLanguage
    when "en" then enTranslations
    when "ru" then ruTranslations
