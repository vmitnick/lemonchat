Chat.RoomRoute = Ember.Route.extend
  model: (params) ->
    room = params.name || "index"
    Chat.LiveMessages.create(room: room)
